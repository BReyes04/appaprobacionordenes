﻿using AppAprobacionOrdenes.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace AppAprobacionOrdenes.Controllers
{
    public class Orden_CompraController : Controller
    { 
      
        public String connectionString = "Data Source=192.168.7.2;Initial Catalog=PRUEBAS2;Persist Security Info=True;User ID=sa;Password=jda;MultipleActiveResultSets=true ";
        // GET: Orden_Compra
        public ActionResult Index()
        {
            //String connectionString = "Data Source=192.168.7.2;Initial Catalog=PRUEBAS;Persist Security Info=True;User ID=sa;Password=jda";

            var model = new List<Orden_Compra>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {


              
                String sql = "SELECT * FROM DBO.ORDENES_COMPRA";
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var ordenes = new Orden_Compra();
                    ordenes.PAIS = rdr["PAIS"].ToString();
                    ordenes.MONEDA = rdr["MONEDA"].ToString();
                    ordenes.NOMBRE = rdr["NOMBRE"].ToString();
                    ordenes.PROVEEDOR = rdr["PROVEEDOR"].ToString();
                    ordenes.TOTAL_MERCADERIA = Convert.ToDecimal( rdr["TOTAL_MERCADERIA"]);
                    ordenes.USUARIO = rdr["USUARIO"].ToString();
                    ordenes.DEPARTAMENTO = rdr["DESCRIPCION"].ToString();
                    ordenes.ESTADO = rdr["ESTADO"].ToString();
                    ordenes.USUARIO_APROBO = rdr["USUARIO_APROBO"].ToString();
                    model.Add(ordenes);
                }
                rdr.Close();
            }

            return View(model);
        }

        // GET: Orden_Compra/Details/5
        public ActionResult Details(string pais, string oc)
        {

            return View(ObjOrden(pais, oc));
    }
        public ActionResult Rechazada()
        {

            return View();
        }

        public Orden_Compra ObjOrden(string pais, string oc)
        {
            var ordenes = new Orden_Compra();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {



                String sql = "SELECT * FROM DBO.ORDENES_COMPRA where pais='" + pais.ToString() + "' and ORDEN_COMPRA='" + oc.ToString() + "' ";
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {

                    ordenes.ORDEN_COMPRA = rdr["ORDEN_COMPRA"].ToString();
                    ordenes.PAIS = rdr["PAIS"].ToString();
                    ordenes.MONEDA = rdr["MONEDA"].ToString();
                    ordenes.NOMBRE = rdr["NOMBRE"].ToString();
                    ordenes.PROVEEDOR = rdr["PROVEEDOR"].ToString();
                    ordenes.TOTAL_MERCADERIA = Convert.ToDecimal(rdr["TOTAL_MERCADERIA"]);
                    ordenes.USUARIO = rdr["USUARIO"].ToString();
                    ordenes.DEPARTAMENTO = rdr["DESCRIPCION"].ToString();
                    ordenes.ESTADO = rdr["ESTADO"].ToString();
                    ordenes.USUARIO_APROBO = rdr["USUARIO_APROBO"].ToString();

                }
                rdr.Close();

            }
            //   var lineas = new Linea_OC();
            var Linea_OC = new List<Linea_OC>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {



                String sql = "SELECT * FROM DBO.ORDENES_COMPRA_LINEA where pais='" + pais.ToString() + "' and ORDEN_COMPRA='" + oc.ToString() + "' ";
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Linea_OC.Add(new Linea_OC() { ORDEN_COMPRA = rdr["ORDEN_COMPRA"].ToString(), PAIS = rdr["PAIS"].ToString(), ORDEN_COMPRA_LINEA = rdr["ORDEN_COMPRA_LINEA"].ToString(), ARTICULO = rdr["ARTICULO"].ToString(), BODEGA = rdr["BODEGA"].ToString(), DESCRIPCION = rdr["DESCRIPCION"].ToString(), CANTIDAD_ORDENADA = Convert.ToInt32(rdr["CANTIDAD_ORDENADA"]), PRECIO_UNITARIO = Convert.ToDecimal(rdr["PRECIO_UNITARIO"]) });
                }

                rdr.Close();
            }


            Orden_Compra ordenescompra = new Orden_Compra()
            {
                PAIS = ordenes.PAIS,
                ORDEN_COMPRA = ordenes.ORDEN_COMPRA,
                USUARIO = ordenes.USUARIO,
                PROVEEDOR = ordenes.PROVEEDOR,
                TOTAL_MERCADERIA = ordenes.TOTAL_MERCADERIA,
                MONEDA = ordenes.MONEDA,
                NOMBRE = ordenes.NOMBRE,
                ESTADO = ordenes.ESTADO,
                DEPARTAMENTO = ordenes.DEPARTAMENTO,
                USUARIO_APROBO=ordenes.USUARIO_APROBO,
                LineasdeOrdenes = Linea_OC.ToList(),
            };
            return ordenescompra;
        }

    // GET: Orden_Compra/Create
    public ActionResult Create()
        {
            return View();
        }

        // GET: Orden_Compra/Aprobar
        public ActionResult Aprobar(string pais, string oc)
        {
            var orden_compra= ObjOrden(pais, oc);
            string compa="";

            switch (orden_compra.PAIS)
            {
                case "ELSALVADOR":
                    compa = "EMASAL";
                    break;
                case "COSTARICA":
                    compa = "EMASALCR";
                    break;
                case "GUATEMALA":
                    compa = "EMASALGT";
                    break;
                case "HONDURAS":
                    compa = "EMASALHN";
                    break;
                case "NICARAGUA":
                    compa = "EMASALNI";
                    break;
                case "PANAMA":
                    compa = "EMASALPTY";
                    break;
            }


            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                // Start a local transaction.
                SqlTransaction sqlTran = conn.BeginTransaction();
                //SqlTransaction sqlTran3 = conn.BeginTransaction();
                //SqlTransaction sqlTran4 = conn.BeginTransaction();
                //SqlTransaction sqlTran5 = conn.BeginTransaction();
                //SqlTransaction sqlTran7 = conn.BeginTransaction();
                //SqlCommand cmd = conn.CreateCommand();
                //SqlCommand cmd3 = conn.CreateCommand();
                //SqlCommand cmd4 = conn.CreateCommand();
                //SqlCommand cmd5 = conn.CreateCommand();
                //SqlCommand cmd7 = conn.CreateCommand();
                // Enlist a command in the current transaction.
                SqlCommand command = conn.CreateCommand();
                command.Transaction = sqlTran;
                try
                {
                 

                    // Commit the transaction.
                   
                    command.CommandText = "UPDATE "+compa.ToString()+".orden_compra SET estado='E' WHERE orden_compra ='"+orden_compra.ORDEN_COMPRA.ToString()+"'";
                    command.ExecuteNonQuery();
                    command.CommandText  = "SELECT ud.USUARIO FROM " + compa.ToString() + ".USUARIOS_DEPARTAMENTO ud   WHERE ud.DEPARTAMENTO=(SELECT oc.DEPARTAMENTO FROM " + compa.ToString() + ".ORDEN_COMPRA oc WHERE oc.ORDEN_COMPRA='" + orden_compra.ORDEN_COMPRA.ToString() + "')";
                    command.ExecuteNonQuery();
                    command.CommandText = "UPDATE " + compa.ToString() + ".orden_compra_linea SET estado='E' WHERE orden_compra ='" + orden_compra.ORDEN_COMPRA.ToString() + "'";
                    command.ExecuteNonQuery();
                    // SqlCommand cmd2 = new SqlCommand(sql2, conn);
                    //String sql3="";
                    //String sql5 = "";
                    //String sql6 = "";
                    command.CommandText  = "INSERT INTO " + compa.ToString() + ".ARTICULO_COMPRA(ARTICULO, ULT_PROVEEDOR, ULT_MONEDA, IMPUESTO, DESCRIPCION, IMP1_AFECTA_COSTO, ULT_PREC_UNITARIO, ULT_FECHA_COTIZA, RECIBIR_MAS, USA_CTA_TRANSITO)(SELECT a.ARTICULO, '" + orden_compra.PROVEEDOR.ToString() +"', '"+orden_compra.MONEDA.ToString()+ "', a.IMPUESTO, a.DESCRIPCION, 'N', '0', GETDATE(), 'N', 'N' FROM  "+ compa.ToString() + ".ARTICULO a WHERE a.ARTICULO IN(SELECT ocl.ARTICULO FROM " + compa.ToString() + ".ORDEN_COMPRA_LINEA ocl WHERE ocl.ORDEN_COMPRA = '"+orden_compra.ORDEN_COMPRA.ToString()+"' AND ocl.ARTICULO NOT IN(SELECT ac.ARTICULO FROM "+compa.ToString()+".ARTICULO_COMPRA ac WHERE ac.ARTICULO = OCL.ARTICULO)))";
                    command.ExecuteNonQuery();
                    command.CommandText = "SELECT ud.USUARIO FROM " + compa.ToString() + ".USUARIOS_DEPARTAMENTO ud   WHERE ud.DEPARTAMENTO=(SELECT oc.DEPARTAMENTO FROM " + compa.ToString() + ".ORDEN_COMPRA oc WHERE oc.ORDEN_COMPRA='" + orden_compra.ORDEN_COMPRA.ToString() + "')";
                    // using (SqlDataReader rdr = command.ExecuteReader())
                    //  {
                    //     while (rdr.Read())
                    //     {
                    var usurario = command.ExecuteScalar();//The 0 stands for "the 0'th column", so the first column of the result.
                            command.CommandText  = "INSERT INTO " + compa.ToString() + ".usuarios_aprob_oc (orden_compra, usuario, fecha_aprob) VALUES ('" + orden_compra.ORDEN_COMPRA.ToString() + "','" + usurario + "', GETDATE() )";
                            command.ExecuteNonQuery();
                            command.CommandText  = "UPDATE " + compa.ToString() + ".orden_compra SET confirmada = 'S',usuario_confirma='"+usurario+ "',fecha_hora_confir=GETDATE() WHERE orden_compra='" + orden_compra.ORDEN_COMPRA.ToString() + "'";
                            command.ExecuteNonQuery();
                    //    }
                   //     rdr.Close();
                //    }
                    int i=0;
                    int c = orden_compra.LineasdeOrdenes.Count();
                    while (i <c)
                    {

                        command.CommandText = "UPDATE " + compa.ToString() + ".existencia_bodega SET cant_transito=cant_transito + '"+orden_compra.LineasdeOrdenes[i].CANTIDAD_ORDENADA.ToString() + "' WHERE bodega='"+ orden_compra.LineasdeOrdenes[i].BODEGA.ToString() + "' AND articulo ='" + orden_compra.LineasdeOrdenes[i].ARTICULO.ToString() + "'";
                        command.ExecuteNonQuery();
                        //SqlCommand cmd6 = new SqlCommand(sql6, conn);
                        //cmd6.ExecuteNonQuery();
                        i++;
                    }

                    //String sql3 = "INSERT INTO EMASAL.usuarios_aprob_oc (orden_compra, usuario, fecha_aprob) VALUES ('" + orden_compra.ORDEN_COMPRA.ToString() + "','"+usurario+"', GETDATE() )";
                    //SqlCommand cmd = new SqlCommand(sql, conn);
                    //SqlCommand cmd3 = new SqlCommand(sql3, conn);
                    //SqlCommand cmd4 = new SqlCommand(sql4,conn);
                    //SqlCommand cmd5 = new SqlCommand(sql5, conn);
                    //SqlCommand cmd7 = new SqlCommand(sql7, conn);
                    //cmd7.ExecuteNonQuery();
                    //cmd3.ExecuteNonQuery();
                    //cmd.ExecuteNonQuery();
                    //cmd4.ExecuteNonQuery();
                    //cmd5.ExecuteNonQuery();
                    sqlTran.Commit();
                    TempData["msg"] = "<script>alert('Orden aprobada con Exito');</script>";
                    string dep = orden_compra.DEPARTAMENTO.ToString();

                    if (dep== "PTSMEN" ^ dep== "PTODMAREPM" ^ dep == "LOCM")
                    {
                        if (compa=="SV" ^ compa == "GT" ^ compa == "HN")
                        {
                            EnviarCorreo("sebastian.leyton@emasal.com", orden_compra);
                        }
                        else
                        {
                            EnviarCorreo("roberto.coto@emasal.com", orden_compra);

                        }
                      
                    }
                   
                }
                catch (Exception ex)
                {
                    try
                    {
                        // Attempt to roll back the transaction.
                        TempData["msg"] = "<script>alert('Un Error a ocurrido, Favor informar a su adminsitrador');</script>";
                       
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        // Throws an InvalidOperationException if the connection
                        // is closed or the transaction has already been rolled
                        // back on the server.
                        Console.WriteLine(exRollback.Message);
                    }
                }
            }

            
            return View();
        }

        public void EnviarCorreo(string email, Orden_Compra Oc)
        {

            string correo = email;
           
            var fromAddress = new MailAddress("notificaciones@emasal.com", "From Name");
            //Aqui un if compañaia
            var toAddress = new MailAddress(correo, "To Name");
            const string fromPassword = "ema$al777";
            const string subject = "Orden de compra Aprobada";
            string body = "La ordeb de compra N# " + Oc.ORDEN_COMPRA.ToString() + " Ha sido Aprobada <br> <a href='http://intranet.emasal.com:1111/AprobacionesOc/Orden_Compra/Details?pais=ELSALVADOR&oc="+Oc.ORDEN_COMPRA.ToString()+"'>Clic aqui para mayor detalle </a>";

            var smtp = new SmtpClient
            {
                Host = "smtp.office365.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml=true
            })
            {
                smtp.Send(message);
            }
        }
        // POST: Orden_Compra/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Orden_Compra/Edit/5
        public ActionResult Rechazar(string pais,string oc)
        {
            return View(ObjOrden(pais, oc));
        }

        // POST: Orden_Compra/Edit/5
        [HttpPost]
        public ActionResult Rechazar(FormCollection collection)
        {

            string message = Request.Form["message"];
            string oc = Request.Form["oc"];
            string pais = Request.Form["pais"];
            string compa = "";
            

            switch (pais)
            {
                case "ELSALVADOR":
                    compa = "EMASAL";
                    break;
                case "COSTARICA":
                    compa = "EMASALCR";
                    break;
                case "GUATEMALA":
                    compa = "EMASALGT";
                    break;
                case "HONDURAS":
                    compa = "EMASALHN";
                    break;
                case "NICARAGUA":
                    compa = "EMASALNI";
                    break;
                case "PANAMA":
                    compa = "EMASALPTY";
                    break;
            }

            try
            {
                // TODO: Add update logic here
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlTransaction sqlTran = conn.BeginTransaction();
                    string correo = "";
                    SqlCommand command = conn.CreateCommand();
                    command.Transaction = sqlTran;
                    try
                    {

                        command.CommandText = "UPDATE "+compa+".ORDEN_COMPRA SET ESTADO='A' WHERE ORDEN_COMPRA='"+oc+"'";
                        command.ExecuteNonQuery();
                        command.CommandText = "UPDATE " + compa + ".ORDEN_COMPRA_LINEA SET ESTADO='A' WHERE ORDEN_COMPRA='" + oc + "'";
                        command.ExecuteNonQuery();
                        command.CommandText = "SELECT u.CORREO_ELECTRONICO FROM erpadmin.USUARIO u WHERE u.USUARIO=(SELECT oc.USUARIO FROM "+compa+".ORDEN_COMPRA oc WHERE oc.ORDEN_COMPRA='"+oc+"')";
                        correo = command.ExecuteScalar().ToString();
                        sqlTran.Commit();
                        //  Response.Write("<script>alert('Solicitud Ingresada con Exito')</script>");
                        var fromAddress = new MailAddress("notificaciones@emasal.com", "From Name");
                        //Aqui un if compañaia
                        var toAddress = new MailAddress(correo, "To Name");
                        const string fromPassword = "ema$al777";
                        const string subject = "Orden de compra Rechazada";
                        string body = "La ordeb de compra N# " +oc + " Ha sido Rechaza, Motivo: "+message+"";

                        var smtp = new SmtpClient
                        {
                            Host = "smtp.office365.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                        };
                        using (var message2 = new MailMessage(fromAddress, toAddress)
                        {
                            Subject = subject,
                            Body = body,
                            IsBodyHtml = true
                        })
                        {
                            smtp.Send(message2);
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            sqlTran.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            Console.WriteLine(exRollback.Message);
                        }
                    }
                }


                TempData["msg"] = "<script>alert('Orden Rechazada con Exito');</script>";
                return RedirectToAction("Rechazada");
            }
            catch
            {
                return View();
            }
        }

        // GET: Orden_Compra/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Orden_Compra/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
