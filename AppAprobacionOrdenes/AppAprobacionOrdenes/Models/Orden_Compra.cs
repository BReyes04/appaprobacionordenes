﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppAprobacionOrdenes.Models
{
    public class Orden_Compra
    {
        public string ORDEN_COMPRA { get; set; }
        public string PAIS { get; set; }
        public string USUARIO { get; set; }
        public string PROVEEDOR { get; set; }
        public Decimal TOTAL_MERCADERIA { get; set; }
        public string MONEDA { get; set; }
        public string NOMBRE { get; set; }
        public string DEPARTAMENTO { get; set; }
        public string ESTADO { get; set; }
        public string USUARIO_APROBO { get; set; }


        public List<Linea_OC> LineasdeOrdenes { get; set; }
    }
    public class Linea_OC
    {
        public string PAIS { get; set; }
        public string ORDEN_COMPRA { get; set; }
        public string ORDEN_COMPRA_LINEA { get; set; }
        public string ARTICULO { get; set; }
        public string BODEGA { get; set; }
        public string DESCRIPCION { get; set; }
        public Int32 CANTIDAD_ORDENADA { get; set; }
        public Decimal PRECIO_UNITARIO { get; set; }

    }


}